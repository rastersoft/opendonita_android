/* Copyright 2020 (C) Raster Software Vigo (Sergio Costas)
 *
 * This file is part of OpenDoñita
 *
 * OpenDoñita is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * OpenDoñita is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.rastersoft.opendonita;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity implements ValueCallback<String> {

    private WebView myWebView;
    private uPnP upnp;
    private boolean asking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void askUPnP() {
        this.asking = true;
        // Spinner
        String unencodedHtml = "<html>\n" +
                "<head>\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
                "<style>\n" +
                ".loader {\n" +
                "   border: 16px solid #f3f3f3;\n" +
                "   border-top: 16px solid #3498db;\n" +
                "   border-radius: 50%;\n" +
                "   width: 120px;\n" +
                "   height: 120px;\n" +
                "   margin: auto;\n" +
                "   animation: spin 2s linear infinite;\n" +
                "}\n" +
                "\n" +
                "@keyframes spin {\n" +
                "  0% { transform: rotate(0deg); }\n" +
                "  100% { transform: rotate(360deg); }\n" +
                "}\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div class=\"loader\">\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>\n";
        String encodedHtml = Base64.encodeToString(unencodedHtml.getBytes(), Base64.NO_PADDING);
        this.myWebView.loadData(encodedHtml, "text/html", "base64");

        this.upnp = new uPnP();
        this.upnp.execute(this);
    }

    public void foundServer(String URL) {
        this.asking = false;
        this.myWebView.loadUrl(URL);
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.myWebView = new WebView(this.getApplicationContext());
        this.myWebView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                view.clearHistory();
            }
        });
        setContentView(this.myWebView);
        WebSettings webSettings = this.myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);
        this.myWebView.clearCache(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.myWebView.onResume();
        this.askUPnP();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.myWebView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.myWebView = null;
    }

    @Override
    public void onBackPressed() {
        if (this.asking) {
            super.onBackPressed();
        } else {
            this.myWebView.evaluateJavascript("back_android();", this);
        }
    }

    public void onReceiveValue(String doBack) {
        Log.e("donita", String.format("valor: %s", doBack));
        if (doBack.equals("1")) {
            super.onBackPressed();
        }
    }
}

class uPnP extends AsyncTask<MainActivity, String, String> {

    private DatagramSocket upnpSocket;
    private MainActivity activity;

    @Override
    protected String doInBackground(MainActivity... input) {
        this.activity = input[0];
        try {
            this.upnpSocket = new DatagramSocket();
            this.upnpSocket.setSoTimeout(3000);
        } catch (SocketException e) {
            return "";
        }

        while (true) {
            String request = "M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nMX: 2\r\nST: upnp:rootdevice\r\n\r\n";
            InetAddress addr;
            try {
                addr = InetAddress.getByAddress(new byte[]{(byte) 239, (byte) 255, (byte) 255, (byte) 250});
            } catch (UnknownHostException e) {
                continue;
            }

            DatagramPacket packet = new DatagramPacket(request.getBytes(), request.length(), addr, 1900);
            try {
                this.upnpSocket.send(packet);
            } catch (SocketException e) {
                Log.e("opendonita", String.format("SocketException %s", e.toString()));
            } catch (IOException e) {
                Log.e("opendonita", String.format("IOException %s", e.toString()));
            }
            while (true) {
                byte[] data = new byte[65536];
                DatagramPacket recv = new DatagramPacket(data, 65535);
                try {
                    this.upnpSocket.receive(recv);
                } catch (Exception e) {
                    Log.e("opendonita", String.format("Receive Exception %s", e.toString()));
                    break;
                }
                String received = new String(recv.getData());
                if (received.contains("USN: ca0e5d94-381c-44d4-8a47-8cddbecae0cf")) {
                    return String.format("http://%s", recv.getAddress().toString());
                }
            }
        }
    }

    protected void onPostExecute(String URL) {
        this.activity.foundServer(URL);
    }
}
